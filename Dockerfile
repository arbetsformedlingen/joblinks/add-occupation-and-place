FROM docker.io/library/debian:stable-20240211-slim

WORKDIR /data

COPY add-occupation-and-place.sh /usr/local/bin/

RUN apt-get update -y && \
        apt-get install -y jq && \
        apt-get install -y parallel && \
        apt-get install -y curl && \
        chmod a+rx /usr/local/bin/add-occupation-and-place.sh

ENTRYPOINT ["/usr/local/bin/add-occupation-and-place.sh"]
