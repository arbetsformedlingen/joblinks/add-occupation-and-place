#!/usr/bin/env bash

# Set variable input_file to either $1 or /dev/stdin, in case $1 is empty
# Note that this assumes that you are expecting the file name to operate on on $1
input_file="${1:-/dev/stdin}"

[[ -n "$APIKEY" ]] || { echo "**** missing setting 'APIKEY'"; exit 1; }

# Probalby unnessacary
cat "$input_file" |\
    jq -c '.' > ads.jsonl

# Prepare for request and response files
mkdir chunks
mkdir requests
mkdir responses

# https://jobad-enrichments-api.jobtechdev.se/ is a RPC service for job ad enrichments
# Input is a array (Maximum around 200) of documents specific for that service
# Split the INPUT of ads to smaller request chunks
< ads.jsonl parallel -N100 --pipe --cat cp {} chunks/request{\#}

# Create request files from chunks
find chunks -type f -print0 |\
  parallel -0 "cat {} | jq -sc '. | map({doc_id:.id ,doc_headline:.originalJobPosting.title,doc_text:.originalJobPosting.description}) | {documents_input:.,include_terms_info:false,include_sentences:false,include_synonyms:false,include_misspelled_synonyms:false}' > requests/{#}"

# Call RPC service in parallel
find requests -type f -print0 |\
  parallel -0 curl -s -X POST -H "api-key:${APIKEY}" -H 'Content-Type:application/json' https://jobad-enrichments-test-api.jobtechdev.se/enrichtextdocumentsbinary -d @{} ">" responses/{\#}.out

# Concat response files
find responses -type f -print0 |\
    xargs -0 cat |\
    jq -c '.[] | {id:.doc_id,yrke:.enriched_candidates.occupations,ort:.enriched_candidates.geos}' > id_occupation_geo.json

# Extract occupation and place then add them to the job file
jq -nc --argfile o1 ads.jsonl --argfile o2 id_occupation_geo.json '$o1 | range(0; length) | $o1[.] * {yrke:$o2[.].yrke,ort:$o2[.].ort}'


# Reasonably safe check to see if files exist matching glob pattern
function files_exist() {
    local pattern="${1}"
    for f in $pattern; do [ -e "$f" ] && return 0 || return 1; done
}

# Dump any error files to stderr
if files_exist "responses/*.err"; then
    echo "**** got curl errors:" >&2
    cat responses/*.err >&2
fi
