#!/usr/bin/env bash

# Docker image name to build and run
docker_img=joblinks/add-occupation-and-place

# This script's directory
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# get api key
. "${script_dir}"/../../secrets/enrich/secrets.sh || exit 1

if [ "$1" = "--build" ]; then
    exec docker build --no-cache -f "${script_dir}"/Dockerfile -t "${docker_img}" "${script_dir}"
fi


if [ "$1" = "--clean" ]; then
    docker image inspect "${docker_img}" 1>/dev/null 2>/dev/null &&\
        docker image rm "${docker_img}"
    exit
fi


docker image inspect "${docker_img}" 1>/dev/null 2>/dev/null ||\
    { echo "**** Build docker image ${docker_img} first (run $0 --build)">&2; exit 1; }


# Run the application
docker run --env APIKEY="${APIKEY}" --rm -i "${docker_img}"
